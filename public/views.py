
from django.views.generic import TemplateView
from django.shortcuts import render

# Create your views here.

class PublicIndexView(TemplateView):
    template_name = "public/index.html"


class PublicDeveloperView(TemplateView):
    template_name = "public/developers.html"
