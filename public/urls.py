from django.urls import path

from .views import PublicIndexView, PublicDeveloperView


urlpatterns = [
    path('', PublicIndexView.as_view(), name='index'),
    path('developers', PublicDeveloperView.as_view(), name='developers'),
]
