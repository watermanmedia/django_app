from django.test import Client, TestCase
from accounts.models import User
from experiments.models import Experiment


client = Client()


class TestExperimentViews(TestCase):

    def setUp(self):
        self.credentials = {
            'username': 'testdev@biggsplit.com',
            'password': 'fakepassword',
        }
        self.user = User.objects.create_user(**self.credentials)
        self.experiment = Experiment.objects.create(
            user=self.user,
            name="Test_experiment_xyz"
        )
        client.post('/login/', self.credentials)

    def test_index_renders(self):
        resp = client.get('/experiments', follow=True)
        self.assertTrue(resp.status_code == 200)
        self.assertContains(resp, self.experiment.name)

    def test_details_render(self):
        resp = client.get('/experiments/{}'.format(self.experiment.id))
        self.assertContains(resp, self.experiment.name)
