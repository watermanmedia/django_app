from django.test import TestCase, Client
from accounts.models import User


client = Client()


class PublicViewTest(TestCase):

    def setUp(self):
        self.credentials = {
            'username': 'testdev@biggsplit.com',
            'password': 'fakepassword',
        }
        self.user = User.objects.create_user(**self.credentials)

    def test_login_get(self):
        resp = client.get('/login/', follow=True)
        self.assertTrue(resp.status_code == 200)

    def test_login_post(self):
        resp = client.post('/login/', self.credentials, follow=True)
        self.assertIn(self.user, User.objects.all())
        self.assertEqual(200, resp.status_code)
        self.assertContains(resp, "Log out")

    def test_index_get(self):
        resp = client.get('/')
        self.assertTrue("Dashboard" not in resp.content.decode('utf8'))
        self.assertTrue(resp.status_code == 200)
        client.post('/login/', self.credentials)
        resp = client.get('/')
        self.assertTrue("Dashboard" in resp.content.decode('utf8'))

    def test_signup_get(self):
        resp = client.get('/accounts/create/')
        self.assertEqual(resp.status_code, 200)
        self.assertContains(resp, 'email')
        self.assertContains(resp, 'password')

    def test_signup_post(self):
        credentials = {'email': 'tjw1991@gmail.com', 'password': 'fake'}
        resp = client.post('/accounts/create/', credentials, follow=True)
        self.assertTrue(resp.status_code == 200)
        user = User.objects.filter(email='tjw1991@gmail.com')
        self.assertTrue(user)

    def test_developer_get(self):
        resp = client.get('/developers', follow=True)
        self.assertTrue(resp.status_code == 200)
