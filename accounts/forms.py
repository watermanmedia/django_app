from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import get_user_model


class CustomUserCreationForm(UserCreationForm):
    password = forms.CharField(label="Password", widget=forms.PasswordInput)
    email = forms.CharField(label="Email", widget=forms.EmailInput)
    password1 = None
    password2 = None

    class Meta:
        model = get_user_model()
        fields = ('email', 'password')

    def save(self, commit=True):
        email = self.cleaned_data['email']
        password = self.cleaned_data['password']
        user = get_user_model().objects.create_user(email=email)
        user.set_password(password)
        user.save()
        return user
