from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth import get_user_model
from django.shortcuts import render
from django.views.generic import TemplateView, CreateView
from django.urls import reverse

from .forms import CustomUserCreationForm


class ShowView(LoginRequiredMixin, TemplateView):
    template_name = 'accounts/show.html'


class CreateUserView(CreateView):
    form_class = CustomUserCreationForm
    model = get_user_model()
    template_name = 'accounts/create.html'
    success_url = '/experiments/'
    # fields = ['email', 'password']
