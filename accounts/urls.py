from django.urls import path, include
from .views import ShowView, CreateUserView

urlpatterns = [
    path('accounts/', ShowView.as_view(), name='account_show'),
    path('accounts/create/', CreateUserView.as_view(), name='signup'),
    path('', include('django.contrib.auth.urls')),
    path('accounts/', include('django.contrib.auth.urls')),
]
