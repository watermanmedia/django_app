from django.db import models
from django.contrib.auth.models import AbstractUser, UserManager


class UserManager(UserManager):
    def create_user(self, email=None, password=None, username=None, **extra_fields):
        email = self.normalize_email(email) or username
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user


class User(AbstractUser):
    def __str__(self):
        return self.email

    objects = UserManager()

    username = ''
    first_name = ''
    last_name = ''

    email = models.EmailField(blank=False, unique=True)

    EMAIL_FIELD = 'email'
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []
