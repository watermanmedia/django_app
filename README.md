# Set up

Start the containers.

```
docker-compose up -d
```

Run migrations.

```
docker-compose exec web python manage.py migrate
```

Restart web container if necessary.

```
docker-compose restart web
```

# Deployment

Push to heroku.

```
git push heroku master
```

# TODOs

Ops:
- Set up CI/CD on travis
- Staticfiles deployed to S3 & loading
- email set up installed
- enable heroku ps:exec https://devcenter.heroku.com/articles/exec#using-with-docker
- add a license

BE:
- CRUD views for experiment creation, outcome, exposure
- split test calculator for each experiment
- API key on user model for django rest framework routes
- Account show view, require login and show something more useful, also T1 link

FE:
- Navbar
- over ride logout redirect url
