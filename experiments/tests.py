from django.test import TestCase

from .models import Experiment, Exposure, Outcome
from accounts.models import User


class TestExperimentModel(TestCase):
    def setUp(self):
        user = User.objects.create_user('test@gmail.com', 'None')
        self.e = Experiment(name='testexp', user=user)
        self.e.save()
        exposure = Exposure(experiment=self.e, segment='control', target_id='a')
        exposure2 = Exposure(experiment=self.e, segment='control', target_id='b')
        exposure3 = Exposure(experiment=self.e, segment='experiment', target_id='c')
        outcome = Outcome(experiment=self.e, target_id='c')
        exposure.save()
        exposure2.save()
        exposure3.save()
        outcome.save()

    def test_load_model_dataframe(self):
        df = self.e.load_model_dataframe()
        self.assertEqual(len(df), 3)

    def test_load_model(self):
        model = self.e.load_model()
        self.assertEqual(model.nobs, 3)

    def test_set_model(self):
        model = self.e.set_model()
        for l, r in zip(model.pvalues, self.e.load_model().pvalues):
            self.assertEqual(l, r)
