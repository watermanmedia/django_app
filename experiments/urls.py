from django.urls import path

from .views import (
    ExperimentListView, ExperimentDetailView, ExperimentCreateView,
    ExperimentDeleteView, ExposuresListView, OutcomesListView
)


urlpatterns = [
    path('', ExperimentListView.as_view(), name='experiment_list'),
    path('<int:pk>', ExperimentDetailView.as_view(), name='experiment_detail'),
    path('create', ExperimentCreateView.as_view(), name='experiment_create'),
    path('delete/<int:pk>', ExperimentDeleteView.as_view(), name='experiment_delete'),
    path('<int:pk>/exposures', ExposuresListView.as_view(), name='experiment_exposures'),
    path('<int:pk>/outcomes', OutcomesListView.as_view(), name='experiment_outcomes'),
]
