from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView, DetailView, CreateView, DeleteView
from django.urls import reverse_lazy
from django.http import HttpResponseRedirect, Http404
from django.shortcuts import get_object_or_404

from .models import Experiment, Outcome, Exposure
from .forms import ExperimentCreateForm


class ExperimentListView(LoginRequiredMixin, ListView):
    model = Experiment
    template_name = 'experiments/list.html'
    context_object_name = 'experiments'

    def get_queryset(self):
        experiments = Experiment.objects.filter(user=self.request.user)
        return experiments


class ExperimentDetailView(LoginRequiredMixin, DetailView):
    model = Experiment
    template_name = 'experiments/detail.html'
    context_object_name = 'experiment'

    def get_object(self, **kwargs):
        user = self.request.user
        experiments = Experiment.objects.filter(user=user)
        return get_object_or_404(experiments, id=self.kwargs.get('pk'))


class ExperimentCreateView(LoginRequiredMixin, CreateView):
    form_class = ExperimentCreateForm
    template_name = 'experiments/create.html'

    def get_success_url(self):
        return reverse_lazy('experiment_list')

    def form_valid(self, form):
        experiment = form.save(commit=False)
        experiment.user = self.request.user
        experiment.save()
        return HttpResponseRedirect(self.get_success_url())


class ExperimentDeleteView(LoginRequiredMixin, DeleteView):
    model = Experiment
    success_url = reverse_lazy('experiment_list')

    def get_queryset(self, *args, **kwargs):
        return Experiment.objects.all().filter(user=self.request.user)


class ExposuresListView(LoginRequiredMixin, ListView):
    model = Exposure
    template_name = 'experiments/exposures/list.html'
    context_object_name = 'exposures'

    def get_queryset(self, *args, **kwargs):
        experiment = Experiment.objects\
                               .filter(user=self.request.user)\
                               .filter(id=self.kwargs['pk'])\
                               .first()
        if experiment:
            return experiment.exposure_set.all()
        else:
            raise Http404()


class OutcomesListView(LoginRequiredMixin, ListView):
    model = Outcome
    template_name = 'experiments/outcomes/list.html'
    context_object_name = 'outcomes'
    allow_empty = True

    def get_queryset(self, *args, **kwargs):
        experiment = Experiment.objects\
                               .filter(user=self.request.user)\
                               .filter(id=self.kwargs['pk'])\
                               .first()
        if experiment:
            return experiment.outcome_set.all()
        else:
            raise Http404()
