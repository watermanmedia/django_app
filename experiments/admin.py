from django.contrib import admin

from .models import Experiment, Exposure, Outcome


admin.site.register(Experiment)
admin.site.register(Exposure)
admin.site.register(Outcome)
