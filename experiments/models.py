import pickle

from django.contrib.auth import get_user_model
from django.utils import timezone
from django.db import models, connection
import pandas as pd
import statsmodels.formula.api as smf


class Experiment(models.Model):
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
    model_cache = models.BinaryField(null=True)
    created_at = models.DateTimeField(default=timezone.now)
    updated_at = models.DateTimeField(default=timezone.now)

    def load_model_sql(self):
        sql = """
        SELECT
            e.target_id,
            e.segment as segment,
            o.created_at as outcome_at
        from experiments_exposure e
        left join experiments_outcome o
            on e.target_id = o.target_id
        where e.experiment_id = %s"""

        return sql

    def load_model_cursor(self):
        cursor = connection.cursor()
        sql = self.load_model_sql()
        cursor.execute(sql, params=[self.id])
        return cursor

    def load_model_dataframe(self):
        cursor = self.load_model_cursor()
        data = cursor.fetchall()
        columns = [c.name for c in cursor.description]
        index = 'target_id'
        df = pd.DataFrame.from_records(data=data, columns=columns, index=index)
        return df

    def load_model(self, cache=True):
        if cache and self.model_cache:
            return pickle.loads(self.model_cache)
        elif cache and not self.model_cache:
            return self.set_model()
        else:
            df = self.load_model_dataframe()
            df['outcome_at'] = pd.notnull(df.outcome_at).astype(int)
            df.reset_index(drop=True)
            model = smf.ols(formula='outcome_at ~ C(segment)', data=df).fit()
            return model

    def set_model(self):
        model = self.load_model(cache=False)
        self.model_cache = pickle.dumps(model)
        return model


class Exposure(models.Model):
    experiment = models.ForeignKey(Experiment, on_delete=models.CASCADE)
    target_id = models.CharField(max_length=255)
    created_at = models.DateTimeField(default=timezone.now)
    updated_at = models.DateTimeField(default=timezone.now)
    segment = models.CharField(max_length=255)


class Outcome(models.Model):
    experiment = models.ForeignKey(Experiment, on_delete=models.CASCADE)
    target_id = models.CharField(max_length=255)
    created_at = models.DateTimeField(default=timezone.now)
    updated_at = models.DateTimeField(default=timezone.now)
