from django.forms import ModelForm

from .models import Exposure, Outcome, Experiment


class ExperimentCreateForm(ModelForm):

    class Meta:
        model = Experiment
        fields = ['name']


class OutcomeCreateForm(ModelForm):
    class Meta:
        model = Outcome
        fields = ['target_id']


class ExposureCreateForm(ModelForm):
    class Meta:
        model = Exposure
        fields = [
            'segment',
            'target_id',
        ]
