FROM python:3.6

RUN apt-get update && apt-get install -y postgresql-client

RUN pip install \
  django==2.1 \
  psycopg2==2.7 \
  djangorestframework==3.9 \
  pandas==0.24 \
  scipy==1.2 \
  statsmodels==0.9 \
  dj-database-url \
  ipython \
  django-crispy-forms

COPY . /django_app
WORKDIR /django_app

CMD python manage.py runserver 0.0.0.0:${PORT}
